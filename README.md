ITxPT(R)2019 specifications
# S02 - Onboard Architecture specifications
XSD files and XML examples are currently available for each of the following services according to ITxPT S02v2.1.0 specification (available in https://wiki.itxpt.org/index.php?title=ITxPT_Technical_Specifications):
- ModuleInventory
- GNSSLocation
- FMStoIP
- VEHICLEtoIP
- AVMS
- APC
